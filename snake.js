var Snake = {};

Snake.gridWidth = 6;
Snake.gridHeight = 6;
Snake.gameSpeedList = [340, 320, 300, 280, 260, 240, 220, 200, 180, 160];
Snake.intervalId;
Snake.inputs = [];

Snake.init = function() {
    this.canvas = document.getElementById("canvas");
    this.canvas.width = this.canvas.clientWidth;
    this.canvas.height = this.canvas.clientHeight;
    this.ctx = this.canvas.getContext("2d");

    document.getElementById("restart").addEventListener("click", function() {
        Snake.gameReset();
    })

    this.gameReset();
}

Snake.updateScore = function() {
    this.gameScore = (Snake.segments.length - 3) * 20 - Snake.gameSteps;
    if (this.gameState === "game-over" || this.gameState === "game-won") {
        this.gameScore += this.gameLives * 20
    }
    document.getElementById("score").innerText = this.gameScore;
}

Snake.gameReset = function() {
    this.gameState = "ready";
    window.clearInterval(this.intervalId);
    this.segments = [
        {x: 5, y: 5},
        {x: 4, y: 5},
        {x: 3, y: 5},
    ];
    this.food = this.getRandomEmptyCell();
    this.lastDirection = "right";
    this.nextDirection = "right";
    this.gameSpeed = this.gameSpeedList[0];
    document.getElementById("speed").innerText = 0;
    this.gameSteps = 0;
    document.getElementById("steps").innerText = this.gameSteps;
    this.updateScore();
    this.setLives(5);
    this.updateFoodLeft();
    document.getElementById("message").innerText = "";
    let gameWindow = document.querySelector(".game-window")
    gameWindow.classList.remove("game-over");
    gameWindow.classList.remove("game-won");
    this.canvas.classList.remove("death");
    this.draw();
}

Snake.gameStart = function() {
    this.updateGameSpeed();
    this.gameState = "active";
}

Snake.gamePause = function() {
    window.clearInterval(this.intervalId);
    this.gameState = "paused";
}

Snake.gameOver = function() {
    window.clearInterval(this.intervalId);
    this.gameState = "game-over";
    this.updateScore();
    document.getElementById("message").innerText = "GAME OVER";
    document.querySelector(".game-window").classList.add("game-over");
}

Snake.gameWon = function() {
    window.clearInterval(this.intervalId);
    this.gameState = "game-won";
    this.updateScore();
    document.getElementById("message").innerText = "YOU WON!!!";
    document.querySelector(".game-window").classList.add("game-won");
}

Snake.keydown = function(keyboardEvent) {
    let arrowPressed = false;
    let queueInput = function(i) {
        if (Snake.inputs.length < 4 &&
            !Snake.inputs.includes(i)) {
                Snake.inputs.push(i)
            }
    }
    if (keyboardEvent.key == "ArrowLeft") {
        queueInput("left");
        arrowPressed = true;
    }
    else if (keyboardEvent.key == "ArrowRight") {
        queueInput("right");
        arrowPressed = true;
    }
    else if (keyboardEvent.key == "ArrowUp") {
        queueInput("up");
        arrowPressed = true;
    }
    else if (keyboardEvent.key == "ArrowDown") {
        queueInput("down");
        arrowPressed = true;
    }

    if (arrowPressed) {
        keyboardEvent.preventDefault();       
        if (Snake.gameState == "ready" ||
            this.gameState == "paused") {
            this.gameStart();
        }
    }
    
    if (keyboardEvent.key == "Escape" && this.gameState == "active") {
        this.gamePause();
    }
}

Snake.step = function() {
    let head = this.segments[0];
    let next = { x: undefined, y: undefined};
    let opposites = {
        "left": "right",
        "right": "left",
        "up": "down",
        "down": "up"
    }
    let loop = true;
    while (loop && this.inputs.length) {
        let input = this.inputs.shift();
        if (input && (input != this.lastDirection || this.lastDirection != this.nextDirection) && input != opposites[this.lastDirection]) {
            this.nextDirection = input;
            loop = false;
        }
    }
    switch (this.nextDirection) {
        case "right":
            next.x = head.x + 1;
            next.y = head.y;
            break;
            
        case "left":
            next.x = head.x - 1;
            next.y = head.y;
            break;
            
        case "down":
            next.x = head.x;
            next.y = head.y + 1;
            break;
            
        case "up":
            next.x = head.x;
            next.y = head.y - 1;
            break;
    }
    Snake.wrapCell(next);
    if (this.segments.some(seg => seg.x == next.x && seg.y == next.y)) {
        this.death();
    }
    else {
        this.flash = false;
        this.gameSteps++;
        document.getElementById("steps").innerText = this.gameSteps;
        this.segments.unshift(next);
        this.lastDirection = this.nextDirection;
        if (next.x == this.food.x && next.y == this.food.y) {
            this.foodCollected();
            if (this.segments.length >= this.gridHeight * this.gridWidth){
                this.gameWon();
            }
        }
        else {
            this.segments.pop();
        }
    }
    this.draw();
}

Snake.draw = function() {
    this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
    this.ctx.strokeStyle = 'rgb(0, 0, 0)';
    this.ctx.strokeWidth = '1px';

    let cellSizeX = this.canvas.width / this.gridWidth;
    let cellSizeY = this.canvas.height / this.gridHeight;
    for (let x=1; x<this.gridWidth; x++) {
        this.ctx.beginPath();
        this.ctx.moveTo(cellSizeX*x, 0);
        this.ctx.lineTo(cellSizeX*x, this.canvas.height);
        this.ctx.stroke();
        this.ctx.closePath();
    }

    for (let y=1; y<this.gridHeight; y++) {
        this.ctx.beginPath();
        this.ctx.moveTo(0, cellSizeY*y);
        this.ctx.lineTo(this.canvas.width, cellSizeY*y);
        this.ctx.stroke();
        this.ctx.closePath();
    }

    // draw the food
    if (this.food) {
        this.ctx.fillStyle = 'green';
        this.ctx.fillRect(
            this.food.x * cellSizeX + 4,
            this.food.y * cellSizeY + 4,
            cellSizeX - 8,
            cellSizeY - 8
        )
    }

    // draw the snake body
    this.segments.forEach(function(seg, index) {
        if ((this.gameState == "death" || this.gameState == "game-over") && index == 0) {
            this.ctx.fillStyle = 'red';
        }
        else if (this.gameState == "game-won") {
            this.ctx.fillStyle = 'green';
        }
        else {
            this.ctx.fillStyle = 'orange';
        }
        Snake.ctx.fillRect(
            seg.x * cellSizeX + 4,
            seg.y * cellSizeY + 4,
            cellSizeX - 8,
            cellSizeY - 8
        )
    }.bind(this))
}

Snake.death = function() {
    this.gameState = "death";
    this.canvas.classList.add("death");
    window.clearInterval(this.intervalId);
    if (!this.setLives(-1)) {
        window.setTimeout(function() {
            this.canvas.classList.remove("death");
            this.gamePause();
        }.bind(Snake), 1000)
    }
}

Snake.foodCollected = function() {
    this.updateScore();
    this.food = this.getRandomEmptyCell();
    this.updateFoodLeft();
    this.updateGameSpeed();
}

Snake.updateFoodLeft = function() {
    let foodLeft = this.gridWidth * this.gridHeight - Snake.segments.length;
    document.getElementById("foods").innerText = foodLeft;
}

Snake.updateGameSpeed = function() {
    let foodLeft = this.gridWidth * this.gridHeight - Snake.segments.length;
    window.clearInterval(this.intervalId);
    let maxScore = this.gridHeight * this.gridWidth;
    let index = Math.floor((maxScore - foodLeft) / maxScore * 10);
    let delay = this.gameSpeedList[index];
    this.intervalId = window.setInterval(this.step.bind(this), delay);
    document.getElementById("speed").innerText = index;
}

Snake.setLives = function(count) {
    let isGameOver = false;
    if (count < 0) {
        this.gameLives += count;
    }
    else {
        this.gameLives = count;
    }
    if (this.gameLives <= 0) {
        this.gameLives = 0;
        this.gameOver();
        isGameOver = true;
    }
    document.getElementById("lives").innerText = this.gameLives;
    return isGameOver;
}

Snake.getRandomEmptyCell = function() {
    let freeCells = this.gridWidth * this.gridHeight - Snake.segments.length
    if (freeCells < 1) {
        return null;
    }
    let cell = { x: undefined, y: undefined}
    do {
        cell.x = Math.floor(Math.random() * this.gridWidth);
        cell.y = Math.floor(Math.random() * this.gridHeight);
    }
    while (Snake.segments.some(seg => seg.x == cell.x && seg.y == cell.y));
    return cell;
}

Snake.wrapCell = function(cell) {
    while (cell.x >= Snake.gridWidth) {
        cell.x = cell.x - Snake.gridWidth;
    }
    while (cell.x < 0) {
        cell.x = cell.x + Snake.gridWidth;
    }
    while (cell.y >= Snake.gridHeight) {
        cell.y = cell.y - Snake.gridHeight;
    }
    while (cell.y < 0) {
        cell.y = cell.y + Snake.gridHeight;
    }
    return cell;
}

window.onload = function() {
    this.addEventListener("keydown", Snake.keydown.bind(Snake));
    Snake.init();
}